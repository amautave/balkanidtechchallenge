// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const objToQueryString = (paramsObj: any) => Object.keys(paramsObj).map(key => key + '=' + paramsObj[key]).join('&');
