import { objToQueryString } from "../utils/fetchUtil";

const apiUrl = 'http://localhost:3333';

export const getAttributes = () => fetch(`${apiUrl}/api/attributes`).then(resp => resp.json());

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const search = (params: any = {}) => fetch(`${apiUrl}/api/search?${objToQueryString(params)}`).then(resp => resp.json());
