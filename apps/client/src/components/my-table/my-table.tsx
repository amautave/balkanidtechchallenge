/* eslint-disable @typescript-eslint/no-explicit-any */
import { capitalize } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import React, { useCallback, useState } from 'react';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

export interface MyTableProps {
  attributes: string[];
  data: any[];
}
type Order = 'asc' | 'desc';

export default function MyTable(props: MyTableProps) {
  const classes = useStyles();
  const { attributes, data } = props;
  const [order, setOrder] = useState<Order>('asc');
  const [orderBy, setOrderBy] = useState('name');
  const compareData = useCallback(
    (a, b) => {
      if (typeof a[orderBy] === 'string') {
        return order === 'asc'
          ? a[orderBy].localeCompare(b[orderBy])
          : b[orderBy].localeCompare(a[orderBy]);
      } else {
        const init = order === 'asc' ? a : b;
        const end = order === 'asc' ? b : a;

        if (init[orderBy] < end[orderBy]) {
          return -1;
        }
        if (init[orderBy] > end[orderBy]) {
          return 1;
        }
        return 0;
      }
    },
    [order, orderBy]
  );
  const createSortHandler =
    (attribute: string) => (event: React.MouseEvent<unknown>) => {
      const isAsc = orderBy === attribute && order === 'asc';

      setOrder(isAsc ? 'desc' : 'asc');
      setOrderBy(attribute);
    };

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            {attributes.map((attribute: string, index: number) => (
              <TableCell key={index}>
                <TableSortLabel
                  active={orderBy === attribute}
                  direction={orderBy === attribute ? order : 'asc'}
                  onClick={createSortHandler(attribute)}
                >
                  {capitalize(attribute)}
                </TableSortLabel>
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {data.sort(compareData).map((record: any, rowIndex: number) => (
            <TableRow key={rowIndex}>
              {attributes.map((attribute: string, attrIndex: number) => (
                <TableCell key={attrIndex}>
                  {record[attribute].toString()}
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
