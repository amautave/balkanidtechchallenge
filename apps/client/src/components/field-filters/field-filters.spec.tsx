import { render } from '@testing-library/react';

import FieldFilters from './field-filters';

describe('FieldFilters', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<FieldFilters />);
    expect(baseElement).toBeTruthy();
  });
});
