/* eslint-disable @typescript-eslint/no-explicit-any */
import { Container } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import TextField from '@material-ui/core/TextField';
import { useState } from 'react';
import styles from './field-filters.module.css';

export interface FieldFiltersProps {
  attributes: string[];
  onAttributesChange: (attributesMap: any) => void;
}

const makeAttributesMap = (attributes: string[]): any => {
  const attributesMap: any = {};

  attributes.forEach(
    (attribute: string) =>
      (attributesMap[attribute] = { isPresent: true, search: null })
  );

  return attributesMap;
};

export function FieldFilters(props: FieldFiltersProps) {
  const { attributes, onAttributesChange } = props;
  const [attributesMap, setAttributesMap] = useState<any>(
    makeAttributesMap(attributes)
  );

  const handlecheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = {
      ...attributesMap,
      [event.target.name]: {
        ...attributesMap[event.target.name],
        isPresent: event.target.checked,
      },
    };

    const shouldUpdateAttributesMap =
      Object.values(newValue)
        .map(({ isPresent }: any) => isPresent)
        .filter(Boolean).length >= 1;

    if (shouldUpdateAttributesMap) {
      setAttributesMap(newValue);
      onAttributesChange(newValue);
    }
  };

  const handleTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = {
      ...attributesMap,
      [event.target.name]: {
        ...attributesMap[event.target.name],
        search: event.target.value,
      },
    };

    setAttributesMap(newValue);
    onAttributesChange(newValue);
  };

  return (
    <Container maxWidth="lg">
      <FormGroup row>
        {attributes.map((attribute: string) => (
          <div className={styles['fieldControls']} key={attribute}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={attributesMap?.[attribute].isPresent}
                  name={attribute}
                  onChange={handlecheckboxChange}
                />
              }
              label={attribute}
            />
            <TextField
              label="Search by"
              name={attribute}
              onChange={handleTextChange}
              size="small"
              type="search"
              variant="outlined"
            />
          </div>
        ))}
      </FormGroup>
    </Container>
  );
}

export default FieldFilters;
