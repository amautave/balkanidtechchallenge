/* eslint-disable @typescript-eslint/no-explicit-any */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { useEffect, useState } from 'react';
import FieldFilters from '../components/field-filters/field-filters';
import MyTable from '../components/my-table/my-table';
import { getAttributes, search } from '../services/personService';

function App() {
  const [attributes, setAttributes] = useState<string[]>();
  const [attributesFilter, setAttributesFilter] = useState<any[]>([]);
  const [searchQueryParams, setSearchQueryParams] = useState<any>({});
  const [data, setData] = useState<any[]>([]);

  useEffect(() => {
    getAttributes().then((data: string[]) => {
      setAttributes(data);
      setAttributesFilter(data);
    });
  }, []);

  useEffect(() => {
    search(searchQueryParams).then((data: any[]) => setData(data));
  }, [searchQueryParams]);

  const onFieldFiltersAttributesChange = (attributesMap: any) => {
    const attrs = Object.entries(attributesMap)
      .filter(([, value]: [unknown, any]) => value.isPresent)
      .map(([key]) => key);
    const queryParams = Object.entries(attributesMap).reduce(
      (prev, cur: any) => {
        const [key, { search }] = cur;

        return {
          ...prev,
          ...(search ? { [key]: search } : {}),
        };
      },
      {}
    );

    setAttributesFilter(attrs);
    setSearchQueryParams(queryParams);
  };

  return (
    <>
      <h1>Field Filters</h1>
      {attributes && (
        <FieldFilters
          attributes={attributes}
          onAttributesChange={onFieldFiltersAttributesChange}
        />
      )}
      <h1>Table</h1>
      <MyTable attributes={attributesFilter} data={data} />
    </>
  );
}

export default App;
