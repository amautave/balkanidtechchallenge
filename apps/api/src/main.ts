/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import cors from 'cors';
import express from 'express';
import { getAttributes, search } from './app/services/personService';

const app = express();

app.use(cors());

app.get('/api', (_, res) => {
  res.send({ message: 'ÏWelcome to api!' });
});

app.get('/api/attributes', (_, res) => {
  res.send(getAttributes());
});

app.get('/api/search', (req, res) => {
  const queryParams = req.query;

  res.send(search(queryParams));
});

const port = process.env.port || 3333;
const server = app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}/api`);
});
server.on('error', console.error);
