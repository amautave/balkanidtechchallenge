/* eslint-disable @typescript-eslint/no-explicit-any */
import { people } from "../../assets/people";
import { IPerson } from "../interfaces/IPerson";

export const getAttributes = (): string[] => {
  const attributes = new Set<string>();

  people.forEach((person: IPerson) => {
    Object.keys(person).forEach((key: string) => attributes.add(key));
  });

  return Array.from(attributes);
};

export const search = (queryParams: any): IPerson[] => {
  const queryParamEntries = Object.entries(queryParams);
  const peopleFiltered = !queryParamEntries.length
    ? people
    : people.filter((person: IPerson) => queryParamEntries.every(
      ([queryParamKey, queryParamValue]: [string, string]) => {
        const personValue = person[queryParamKey].toString();

        return personValue == queryParamValue;
      }
    ));

  return peopleFiltered;
}
