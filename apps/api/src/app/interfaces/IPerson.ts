
export interface IPerson {
  age: number;
  busy: boolean;
  happy: boolean;
  healthy: boolean;
  name: string;
}
